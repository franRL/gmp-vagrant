#!/usr/bin/bash

echo "Starting GMP vagrant setup"
yum -y update
yum -y install net-tools wget
yum -y group install "Development Tools"
yum -y install python-devel python-setuptools python-mysql
yum -y install java-1.8.0-openjdk-devel
yum -y install mariadb-server
yum -y install vim
yum -y install gvim
yum -y install xorg-x11-xauth
yum -y install epel-release.noarch
yum -y install tar
yum -y install xorg-x11-xauth
yum -y install tcsh
yum -y install maven

mkdir /home/vagrant/gmp
cd /home/vagrant/gmp
git clone https://github.com/gemini-hlsw/gmp
git clone https://github.com/gemini-hlsw/giapi-glue-cc

echo "configuring external libraries"
export GIAPI_ROOT=/home/vagrant/gmp/giapi-glue-cc
export BOOST_ROOT=/home/vagrant/gmp/giapi-glue-cc/external/boost
cd $GIAPI_ROOT/external/apr-1.3.12
./configure --prefix=$GIAPI_ROOT/external/apr
make && make install
ln -s $GIAPI_ROOT/external/apr-1.3.12 apr-1 

# APR-util
echo "...... APR-uti  ....."
cd $GIAPI_ROOT/external/apr-util-1.3.10 
./configure --with-apr=$GIAPI_ROOT/external/apr --prefix=$GIAPI_ROOT/external/apr-util
make && make install

# ActiveMQ
cd $GIAPI_ROOT/external/activemq-cpp-library-3.4.1 
./configure --prefix=$GIAPI_ROOT/external/activemq-cpp --with-apr=$GIAPI_ROOT/external/apr -with-apr-util=$GIAPI_ROOT/external/apr-util --without-openssl 
make && make install

# log4cxx
echo "...... log4cxx ....."
cd $GIAPI_ROOT/external/apache-log4cxx-0.10.0  
./configure --prefix=$GIAPI_ROOT/external/log4cxx --with-apr=$GIAPI_ROOT/external/apr -with-apr-util=$GIAPI_ROOT/external/apr-util 
make && make install 

# cppunit
echo "...... cppunit ....."
cd $GIAPI_ROOT/external/cppunit-1.12.1 
./configure --prefix=$GIAPI_ROOT/external/cppunit  
make && make install 

echo "...... curl library ......."
cd $GIAPI_ROOT/external/curl-7.21.6/
./configure --prefix=$GIAPI_ROOT/external/libcurl
make && make install

# curlpp
echo "...... curlp ....."
cd $GIAPI_ROOT/external/curlpp-0.7.3 
export CXXFLAGS="-I$GIAPI_ROOT/external/libcurl/include -L$GIAPI_ROOT/external/libcurl/lib"
./configure --prefix=$GIAPI_ROOT/external/curlpp --without-boost
make && make install


echo "configuring giapi-glue"
cd /home/vagrant/gmp/giapi-glue-cc
sed  's/INSTALL.*/INSTALL_DIR := \/home\/vagrant\/gmp\/giapi-glue-cc\/install/g' conf/config.mk > /tmp/config.mk
sed 's/EXTERNAL_LIB.*/EXTERNAL_LIB := \/home\/vagrant\/gmp\/giapi-glue-cc\/external/g' /tmp/config.mk > /tmp/config2.mk
sed 's/DISTRIBUTION_DIR.*/DISTRIBUTION_DIR := \/home\/vagrant\/gmp\/giapi-glue-cc\/dist/g' /tmp/config2.mk >  /tmp/config.mk
cp /tmp/config.mk conf/config.mk

echo "build lib giapi-glue"
mkdir  -p  /gemsoft/opt/gpi/giapi-dist
make && make install
sudo echo ${GIAPI_ROOT}/external/apr/lib >> /etc/ld.so.conf 
sudo echo ${GIAPI_ROOT}/external/apr-util/lib >> /etc/ld.so.conf 
sudo echo ${GIAPI_ROOT}/external/activemq-cpp/lib >> /etc/ld.so.conf 
sudo echo ${GIAPI_ROOT}/external/log4cxx/lib >> /etc/ld.so.conf  
sudo echo ${GIAPI_ROOT}/external/cppunit/lib >> /etc/ld.so.conf 
sudo echo ${GIAPI_ROOT}/external/curlpp/lib >> /etc/ld.so.conf  
sudo echo ${GIAPI_ROOT}/external/libcurl/lib/ >> /etc/ld.so.conf  
sudo echo ${GIAPI_ROOT}/install/lib >> /etc/ld.so.conf 
sudo ldconfig -n /gemsoft/opt/gpi/giapi-dist/lib 
echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/gemsoft/opt/gpi/giapi-dist/lib/:/home/vagrant/gmp/giapi-glue-cc/external/curlpp/lib/" >> /etc/bashrc
chown -R vagrant:vagrant /gemsoft
chown -R vagrant:vagrant ${GIAPI_ROOT}
chmod 755 -R /gemsoft
chmod 755 -R ${GIAPI_ROOT}

echo "export LIBRARY_PATH=$LIBRARY_PATH:${GIAPI_ROOT}/external/apr/lib:${GIAPI_ROOT}/external/apr-util/lib:${GIAPI_ROOT}/external/activemq-cpp/lib:${GIAPI_ROOT}/external/log4cxx/lib:${GIAPI_ROOT}/external/cppunit/lib:${GIAPI_ROOT}/external/curlpp/lib:${GIAPI_ROOT}/external/curlpp/lib:${GIAPI_ROOT}/external/libcurl/lib:${GIAPI_ROOT}/install/lib" >> /home/vagrant/.bashrc

echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${GIAPI_ROOT}/external/apr/lib:${GIAPI_ROOT}/external/apr-util/lib:${GIAPI_ROOT}/external/activemq-cpp/lib:${GIAPI_ROOT}/external/log4cxx/lib:${GIAPI_ROOT}/external/cppunit/lib:${GIAPI_ROOT}/external/curlpp/lib:${GIAPI_ROOT}/external/curlpp/lib:${GIAPI_ROOT}/external/libcurl/lib:${GIAPI_ROOT}/install/lib " >> /home/vagrant/.bashrc

echo "export GIAPI_ROOT=/home/vagrant/gmp/giapi-glue-cc" >> /home/vagrant/.bashrc
echo "export BOOST_ROOT=/home/vagrant/gmp/giapi-glue-cc/external/boost " >> /home/vagrant/.bashrc

cd /home/vagrant/gmp/giapi-glue-cc/src/examples
make 

# This is to run by default in the virtual machine
echo "gmp.hostname=127.0.0.1" > gmp.properties

echo "configuring maven"
cd /home/vagrant/gmp/gmp
# the bb78.. hash commit matchs with the version that I tested and worked well after the 
# modification. 
git checkout bb78e0460cedb4ab33dad86ac7cfa068db37adfe
mvn -Dmaven.test.skip=true install
chown vagrant:vagrant -R /home/vagrant/gmp/gmp

