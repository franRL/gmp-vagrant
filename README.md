# GMP-Vagrant
The main part of the GIAPI environment consists of two pieces: the Gemini Master Process (GMP) and one or more implementations of the language-specific API or GIAPI glue (referred to as API glue). The Gemini Master Process is a Gemini-provided component that provides the bulk of the Gemini features and integration functionality. The language-specific glue is the API layer used by the builder to implement integration functionality. Currently, the glue API is implemented as a shared library in C++, but the idea is implemented the python counterpart. 


## Getting started

This vagrant virtual machine is a fast solution to have an GIAPI environment. For this purpose, the GMP and GIAPI-GLUE are downloaded and configured to be available for use. 

# Start Up

To create and deploy the virtual machine, just run the **vagrant up** command. When you run this command the first time, it will create the virtual machine with centos 7 and download the source code for both GMP and GIAPI-GLUE. The last step of the process, you will have to choose the network interface you want to work with as a bridge. 

Once you have finished the previous program, to enter the virtual machine you have to execute the command **vagrant ssh**.

## Run GMP

In order to deploy the GMP you must be inside the virtual machine (vagrant ssh). Once inside the virtual machine execute the following commands:
- cd /home/vagrant/gmp/gmp/gmp
- mvn pax:run

If you change the source code you have to recompile and for that use the command **mvn -Dmaven.test.skip=true clean; mvn -Dmaven.test.skip=true install

## Run GIAPI-GLUE

The C++ version of GIAPI-GLUE can be found under the directory **/home/vagrant/gmp/giapi-glue-cc** .  The C++ version has a number of examples located in the **/home/vagrant/gmp/giapi-glue-cc/src/examples** directory.  To run the examples, you have to compile the examples executing the **make** command. Then you could run any command, for example the called **./any-command** .


## Quick test

One way to check that GMP is correctly configured and installed is to use the giapi-tester tool. For this example, you need to have started the any_command example in giapi-glue and deployed the GMP beforehand. 

To run giapi-tester you have to go to the directory **/home/vagrant/gmp/gmp/gmp/giapi-tester** and then run the command **java -jar target/giapi-tester.jar -sc INIT -activity PRESET**.
